'use strict';

describe('', function() {

  var module;
  var dependencies;
  dependencies = [];

  var hasModule = function(module) {
  return dependencies.indexOf(module) >= 0;
  };

  beforeEach(function() {

  // Get module
  module = angular.module('login-component');
  dependencies = module.requires;
  });

  it('should load config module', function() {
    expect(hasModule('login-component.config')).to.be.ok;
  });

  

  
  it('should load directives module', function() {
    expect(hasModule('login-component.directives')).to.be.ok;
  });
  

  
  it('should load services module', function() {
    expect(hasModule('login-component.services')).to.be.ok;
  });
  

  
   

});
