(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('login-component.config', [])
      .value('login-component.config', {
          debug: true
      });

  // Modules
  
  angular.module('login-component.directives', []);
  
  
  
  angular.module('login-component.services', []);
  
  
  angular.module('login-component.controllers', []);
  
  angular.module('login-component',
      [
        'login-component.config',
        'login-component.directives',
        'login-component.services',
        'login-component.controllers'
      ]);

})(angular);
