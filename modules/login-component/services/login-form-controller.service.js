/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular
.module('login-component')
.service('LoginFormController', LoginFormController );

LoginFormController.$inject = [];

function LoginFormController()
{
    var $email;

    var $password;

    var $result = {
        error : null,
        message : null
    };

    var $successCallback;

    this.validate = function( email , password , callback )
    {
        $email = email;
        $password = password;

        if( email === undefined )
        {
            $result.message = 'invalid email';
            $result.error = true;
            callback( $result );
            return this;
        }

        if( password === undefined )
        {
            $result.message = 'password cannot be empty';
            $result.error = true;
            callback( $result );
            return this;
        }

        this.processLogin( email , password );
        return this;
    };

    this.success = function( callback )
    {
       $successCallback = callback;
    };

    this.processLogin = function( email , password )
    {
       if( $successCallback !== undefined )
       {
          $successCallback( 'it was successful email: ' + email +  ' password: ' +  password );
       }

        
    };
}