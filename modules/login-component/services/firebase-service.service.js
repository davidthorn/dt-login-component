/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('login-component').service('FirebaseService', FirebaseService );

FirebaseService.$inject = [];

function FirebaseService()
{
    var fb = firebase;

    this.auth = function( email , password , callback )
    {

      var self = this;

       firebase
       .auth()
       .signInWithEmailAndPassword( email , password )
       .then( function(d)
       {
          console.log( 'we have a successfull login' );
          var user = firebase.auth().currentUser;

          if( !user )
          {
              alert('error with logging in');
              return;
          }


          if( user.displayName === undefined || user.displayName === null )
          {
              self.updateProfile( user );
          }

          alert('Logged as ' + user.displayName );

          callback( user );

      } )
      .catch( function(e)
      {
          callback( e );
      } );


    };

    this.updateProfile = function( user )
    {
        user.updateProfile({
          displayName: user.uid
        }).then(function() {
          // Update successful.
          console.log( 'user information updated' );
        }, function(error) {
          console.log( 'user information not updated' );
          console.error( error );
          // An error happened.
        });
    };


}