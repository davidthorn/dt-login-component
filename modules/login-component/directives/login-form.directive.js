/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular
.module('login-component').component( 'loginForm' , {

  controller: LoginFormComponentController,
  controllerAs: 'ctrl',
  bindings: {
      successUrl: '@',
      firebaseInstance: '='
  },
  //templateUrl: '/modules/login-component/directives/login-form.tpl.html',
  template: "<form name=\"userForm\" ng-submit=\"ctrl.formSubmit()\" novalidate>\n\n        <!-- Email -->\n        <div class=\"form-group\" ng-class=\"{ \'has-error\' : userForm.email.$invalid && !userForm.email.$pristine }\">\n            <label>Email*</label>\n            <input type=\"email\" name=\"email\" class=\"form-control\" ng-model=\"ctrl.email\" ng-minlength=\"3\" required>\n             <p ng-show=\"userForm.email.$invalid && !userForm.email.$pristine\" class=\"help-block\">Enter a valid email.</p>\n            <p ng-show=\"userForm.email.$error.minlength\" class=\"help-block\">Email is too short.</p>\n        </div>\n            \n        <!-- EMAIL -->\n        <div class=\"form-group\" ng-class=\"{ \'has-error\' : userForm.password.$invalid && !userForm.password.$pristine }\">\n            <label>Password*</label>\n            <input type=\"password\" ng-minlength=\"8\" name=\"password\" class=\"form-control\" ng-model=\"ctrl.password\" required>\n           \n             <p ng-show=\"userForm.password.$error.minlength\" class=\"help-block\">Password is too short.</p>\n        </div>\n        \n        <button type=\"submit\" class=\"btn btn-primary\" ng-disabled=\"userForm.$invalid\">Submit</button>\n        \n    </form>\n    \n",
  restrict: 'E',

} );

LoginFormComponentController.$inject = [ '$rootScope' ,  '$scope', '$location','LoginFormController','FirebaseService'];

function LoginFormComponentController( $rootScope , $scope , $location , LoginFormController , FirebaseService )
{
    this.successUrl = $scope.successUrl;

    this.firebaseInstance = $scope.firebaseInstance;

    var self = this;

    this.formSubmit = function()
    {

      
        var $email = this.email;
        var $password = this.password;

        LoginFormController.validate( this.email ,  this.password , function( result )
        {
            console.error(result);
        } 
        )
        .success( function( result )
        {

           FirebaseService.auth( $email , $password , function( error )
           {
           
             if( error.code !== undefined )
             {
                throw 'The email address or password is incorrect';
                return;
             } 

              //alert('time to redirect to ' + self.successUrl);
              $location.path( self.successUrl ).replace();
              $rootScope.$apply();
           } );

        } );

    };
}