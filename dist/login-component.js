(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('login-component.config', [])
      .value('login-component.config', {
          debug: true
      });

  // Modules
  
  angular.module('login-component.directives', []);
  
  
  
  angular.module('login-component.services', []);
  
  
  angular.module('login-component.controllers', []);
  
  angular.module('login-component',
      [
        'login-component.config',
        'login-component.directives',
        'login-component.services',
        'login-component.controllers'
      ]);

})(angular);

angular.module("login-component.directives").run(["$templateCache", function($templateCache) {$templateCache.put("login-component/directives/login-form.tpl.html","  <form name=\"userForm\" ng-submit=\"ctrl.formSubmit()\" novalidate>\n\n        <!-- Email -->\n        <div class=\"form-group\" ng-class=\"{ \'has-error\' : userForm.email.$invalid && !userForm.email.$pristine }\">\n            <label>Email*</label>\n            <input type=\"email\" name=\"email\" class=\"form-control\" ng-model=\"ctrl.email\" ng-minlength=\"3\" required>\n             <p ng-show=\"userForm.email.$invalid && !userForm.email.$pristine\" class=\"help-block\">Enter a valid email.</p>\n            <p ng-show=\"userForm.email.$error.minlength\" class=\"help-block\">Email is too short.</p>\n        </div>\n            \n        <!-- EMAIL -->\n        <div class=\"form-group\" ng-class=\"{ \'has-error\' : userForm.password.$invalid && !userForm.password.$pristine }\">\n            <label>Password*</label>\n            <input type=\"password\" ng-minlength=\"8\" name=\"password\" class=\"form-control\" ng-model=\"ctrl.password\" required>\n           \n             <p ng-show=\"userForm.password.$error.minlength\" class=\"help-block\">Password is too short.</p>\n        </div>\n        \n        <button type=\"submit\" class=\"btn btn-primary\" ng-disabled=\"userForm.$invalid\">Submit</button>\n        \n    </form>\n    \n");}]);
/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular
.module('login-component').component( 'loginForm' , {

  controller: LoginFormComponentController,
  controllerAs: 'ctrl',
  bindings: {
      successUrl: '@',
      firebaseInstance: '='
  },
  //templateUrl: '/modules/login-component/directives/login-form.tpl.html',
  template: "<form name=\"userForm\" ng-submit=\"ctrl.formSubmit()\" novalidate>\n\n        <!-- Email -->\n        <div class=\"form-group\" ng-class=\"{ \'has-error\' : userForm.email.$invalid && !userForm.email.$pristine }\">\n            <label>Email*</label>\n            <input type=\"email\" name=\"email\" class=\"form-control\" ng-model=\"ctrl.email\" ng-minlength=\"3\" required>\n             <p ng-show=\"userForm.email.$invalid && !userForm.email.$pristine\" class=\"help-block\">Enter a valid email.</p>\n            <p ng-show=\"userForm.email.$error.minlength\" class=\"help-block\">Email is too short.</p>\n        </div>\n            \n        <!-- EMAIL -->\n        <div class=\"form-group\" ng-class=\"{ \'has-error\' : userForm.password.$invalid && !userForm.password.$pristine }\">\n            <label>Password*</label>\n            <input type=\"password\" ng-minlength=\"8\" name=\"password\" class=\"form-control\" ng-model=\"ctrl.password\" required>\n           \n             <p ng-show=\"userForm.password.$error.minlength\" class=\"help-block\">Password is too short.</p>\n        </div>\n        \n        <button type=\"submit\" class=\"btn btn-primary\" ng-disabled=\"userForm.$invalid\">Submit</button>\n        \n    </form>\n    \n",
  restrict: 'E',

} );

LoginFormComponentController.$inject = [ '$rootScope' ,  '$scope', '$location','LoginFormController','FirebaseService'];

function LoginFormComponentController( $rootScope , $scope , $location , LoginFormController , FirebaseService )
{
    this.successUrl = $scope.successUrl;

    this.firebaseInstance = $scope.firebaseInstance;

    var self = this;

    this.formSubmit = function()
    {

      
        var $email = this.email;
        var $password = this.password;

        LoginFormController.validate( this.email ,  this.password , function( result )
        {
            console.error(result);
        } 
        )
        .success( function( result )
        {

           FirebaseService.auth( $email , $password , function( error )
           {
           
             if( error.code !== undefined )
             {
                throw 'The email address or password is incorrect';
                return;
             } 

              //alert('time to redirect to ' + self.successUrl);
              $location.path( self.successUrl ).replace();
              $rootScope.$apply();
           } );

        } );

    };
}
/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular.module('login-component').service('FirebaseService', FirebaseService );

FirebaseService.$inject = [];

function FirebaseService()
{
    var fb = firebase;

    this.auth = function( email , password , callback )
    {

      var self = this;

       firebase
       .auth()
       .signInWithEmailAndPassword( email , password )
       .then( function(d)
       {
          console.log( 'we have a successfull login' );
          var user = firebase.auth().currentUser;

          if( !user )
          {
              alert('error with logging in');
              return;
          }


          if( user.displayName === undefined || user.displayName === null )
          {
              self.updateProfile( user );
          }

          alert('Logged as ' + user.displayName );

          callback( user );

      } )
      .catch( function(e)
      {
          callback( e );
      } );


    };

    this.updateProfile = function( user )
    {
        user.updateProfile({
          displayName: user.uid
        }).then(function() {
          // Update successful.
          console.log( 'user information updated' );
        }, function(error) {
          console.log( 'user information not updated' );
          console.error( error );
          // An error happened.
        });
    };


}
/**
 * Created by coichedid on 21/04/15.
 */
'use strict';

angular
.module('login-component')
.service('LoginFormController', LoginFormController );

LoginFormController.$inject = [];

function LoginFormController()
{
    var $email;

    var $password;

    var $result = {
        error : null,
        message : null
    };

    var $successCallback;

    this.validate = function( email , password , callback )
    {
        $email = email;
        $password = password;

        if( email === undefined )
        {
            $result.message = 'invalid email';
            $result.error = true;
            callback( $result );
            return this;
        }

        if( password === undefined )
        {
            $result.message = 'password cannot be empty';
            $result.error = true;
            callback( $result );
            return this;
        }

        this.processLogin( email , password );
        return this;
    };

    this.success = function( callback )
    {
       $successCallback = callback;
    };

    this.processLogin = function( email , password )
    {
       if( $successCallback !== undefined )
       {
          $successCallback( 'it was successful email: ' + email +  ' password: ' +  password );
       }

        
    };
}